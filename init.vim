"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""               
"               
"               ██╗   ██╗██╗███╗   ███╗██████╗  ██████╗
"               ██║   ██║██║████╗ ████║██╔══██╗██╔════╝
"               ██║   ██║██║██╔████╔██║██████╔╝██║     
"               ╚██╗ ██╔╝██║██║╚██╔╝██║██╔══██╗██║     
"                ╚████╔╝ ██║██║ ╚═╝ ██║██║  ██║╚██████╗
"                 ╚═══╝  ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝ ╚═════╝
"               
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""     

" By Sr.Estegosaurio

"-->> BASIC CONFIG
set noshowmode			" Hides the mode and that. Not needed since itchyny/lightline.vim is used. (See line X)	
set nocompatible		" Disables compatibility between (n)vim and vi to not have issues.
syntax on			" Turns syntax highligthing on.
set number			" Displays the number of each line.

"	-->>	CURSOR CONFIG
set cursorline			" Highlight cursor line underneath the cursor horizontally.
set cursorcolumn		" Highlight cursor line underneath the cursor vertically.
"	-->>	CURSOR CONFIG END

"	-->>	FILES CONFIG
set wildignore=*.odt,*.png,*.gif,*.pdf,*.pyc,*.jpg,*.desktop,*.key,*.tomb
filetype on			" Enables the autodetection of the file type.




"->>   PLUGINS 
call plug#begin('~/.local/share/nvim/plugged') " Starts the plugins.

Plug 'luochen1990/rainbow'			" Used for colring backets and etc.
Plug 'preservim/nerdtree'			" Shows the tree of directories. (Show/unshow with ctrl-t)
Plug 'itchyny/lightline.vim'			" The little line with info at the bottom.
Plug 'tpope/vim-fugitive'
Plug 'godlygeek/tabular'			" Keeps text aligned, needed for vim-markdown plugin.
Plug 'plasticboy/vim-markdown'			" Markdonw on vim.

" !!
Plug 'ryanoasis/vim-devicons'			" Icons. Always make sure to add this line the very last.

" This line delimits the end of the pulgins.
call plug#end()
"-->>	PLUGINS END




"-->>	EXTRA CONFIG
"	-->>	PLUGIN: 'luochen1990/rainbow' 
let g:rainbow_active = 1			" Activates the 'luochen1990/rainbow' plugin for coloring brackets and so.

"	-->>	PLUGIN: 'preservim/nerdtree'
autocmd VimEnter * NERDTree | wincmd p		" Autostarts the 'preservim/nerdtree' plugin.
"						" Sets the keyblinding for togglin the 'preservim/nerdtree'.						
nnoremap <C-t> :NERDTreeToggle<CR>

"	-->>	PLUGIN: 'itchyny/lightline.vim'
if !has('gui_running')
	set t_Co=256
endif

"	-->>	PLUGIN:	'plasticboy/vim-markdown'
let g:vim_markdown_auto_extension_ext = 'md'
let g:vim_markdown_math = 1			" Idk what it does, but otherway the markdown syntax does not work.





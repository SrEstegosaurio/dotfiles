# ~/.bashrc

# Made by:

#   ____       _____     _                                        _       
#  / ___| _ __| ____|___| |_ ___  __ _  ___  ___  ___  _   _ _ __(_) ___  
#  \___ \| '__|  _| / __| __/ _ \/ _` |/ _ \/ __|/ _ \| | | | '__| |/ _ \ 
#   ___) | |_ | |___\__ \ ||  __/ (_| | (_) \__ \ (_) | |_| | |  | | (_) |
#  |____/|_(_)|_____|___/\__\___|\__, |\___/|___/\___/ \__,_|_|  |_|\___/ 
#                                |___/                                    
#

# <3rmygign@anonaddy.me> E-mail me for anything!


# [COMMANDS]

fastfetch



# [CUSTOM VARIABLES]
# Note: This part REALLY needs custom config.

export EDITOR="*"	# Substitute this by your editor of choice.(vim, nvim, emacs, ed...)


# 	\-> P.D: For the sake of keeping all nice and clean add all your variables up here.




# [ALIASES] 
# Note: [*] --> Needs custom config.

# \-> Improves the overal experience.
alias please="sudo"

# \-> Prevents you from accidentally removing files.
alias rm="rm -i"

# \-> Prevents you from accidentally overwrinting files.
alias cp="cp -i"

# \-> Improves ls command
alias ls="ls --color"

# \-> Makes the procces of using tar not painful.
alias tarxz="tar -Jxvf"
alias targz="tar -zxvf"

# \-> Updates the system.
alias update="sudo pacman -Syu"

# \-> Installs a pacakage.
alias install="sudo pacman -S"

# \-> Searchs a pacakage.
alias search="sudo pacman -Ss"

# \-> Improves man
export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline
export MANPAGER='less -s -M +Gg'       # Shows the % of the document.

# \-> [CUSTOM]


